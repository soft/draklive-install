#!/bin/sh
# Author: mikhailnov <m.novosyolov@rosalinux.ru>
# Re-create XDG user directories, because in live mode systemd user service
# user-dirs-update.service (ROSA's xdg-user-dirs >= 0.17-2) runs earlier than the language is chosen
xdg-user-dirs-update || /usr/bin/xdg-user-dirs-update || :
